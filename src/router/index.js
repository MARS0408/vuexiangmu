import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login/index.vue'
import Home from '@/views/Home/home.vue'
import Welcome from '@/views/Welcome/welcome.vue'
import Users from '@/views/Users/users.vue'
import Rights from '@/views/Rights/rights.vue'
import Roles from '@/views/Rights/roles.vue'
import Cate from '@/views/Goods/cate.vue'
import Params from '@/views/Goods/params.vue'
import List from '@/views/Goods/list.vue'
import Add from '@/views/Goods/add.vue'
import Orders from '@/views/Orders/orders.vue'
import Reports from '@/views/Reports/reports.vue'

Vue.use(VueRouter)
// 解决路由重复报错
// const originalPush = VueRouter.prototype.push
// VueRouter.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => err)
// }
const routes = [
  // 路由重定向
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      { path: '/welcome', component: Welcome },
      { path: '/users', component: Users },
      { path: '/rights', component: Rights },
      { path: '/roles', component: Roles },
      { path: '/categories', component: Cate },
      { path: '/params', component: Params },
      { path: '/goods', component: List },
      { path: '/goods/add', component: Add },
      { path: '/orders', component: Orders },
      { path: '/reports', component: Reports }
    ]
  }
]

const router = new VueRouter({
  routes
})
// 挂在路由导航守卫  触发的事假：进入每一个路由之前
router.beforeEach((to, from, next) => {
  // to:将要访问的路径
  // from代表从哪个路径跳转而来
  // next是一个函数
  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})
export default router
