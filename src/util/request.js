import Vue from 'vue'
import axios from 'axios'
import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'

// 配置请求根域名的方法defaults.baseURL
axios.defaults.baseURL = 'https://www.liulongbin.top:8888/api/private/v1'
// 配置axios请求拦截器
// interceptors：拦截器
// 再发起请求时添加进度条 Nprogress.start()
axios.interceptors.request.use(config => {
  // console.log(config)
  Nprogress.start()
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
// 在请求结束后删除进度条效果Nprogress.done()
// 响应拦截器
axios.interceptors.response.use(res => {
  Nprogress.done()
  return res.data
})
Vue.prototype.axios = axios
