import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import '@/styles/main.css'
import '@/assets/font/iconfont.css'
import '@/util/request.js'
import ZkTable from 'vue-table-with-tree-grid'
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor)
Vue.component('tree-table', ZkTable)
Vue.config.productionTip = false

Vue.filter('formatDate', (data) => {
  const date = new Date(data * 1000)
  const y = date.getFullYear()
  const M = (date.getMonth() + 1).toString().padStart(2, 0)
  const d = date.getDate().toString().padStart(2, 0)
  const h = date.getHours().toString().padStart(2, 0)
  const m = date.getMinutes().toString().padStart(2, 0)
  const s = date.getSeconds().toString().padStart(2, 0)
  return `${y}-${M}-${d} ${h}:${m}:${s}`
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
