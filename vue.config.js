module.exports = {
  publicPath: './',
  chainWebpack: config => {
    // 根据环境设置不同的入口文件
    // config.when 相当于 if 
    // 判断环境为 发布阶段 设置入口文件为main-prod.js
    config.when(process.env.NODE_ENV === 'production', config => {
      config.entry('app').clear().add('./src/main-prod.js')
    })
    // 判断环境为 开发阶段 设置入口文件为main-dev.js
    config.when(process.env.NODE_ENV === 'development', config => {
      config.entry('app').clear().add('./src/main-dev.js')
    })
  }
}